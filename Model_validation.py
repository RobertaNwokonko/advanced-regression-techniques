from xgboost import XGBRegressor
import pandas as pd
from sklearn.metrics import mean_absolute_error


iowa_test_data = 'Dataset1/test.csv'
iowa_train_data = 'Dataset1/train.csv'
iowa_sample_submission = 'Dataset1/sample_submission.csv'

home_test_data = pd.read_csv(iowa_test_data)
home_train_data = pd.read_csv(iowa_train_data)
sample_submission = pd.read_csv(iowa_sample_submission)

# encoding the MSZoning column into 1s and 0s
dummie_test = pd.get_dummies(home_test_data.MSZoning)
merged_test = pd.concat([home_test_data, dummie_test], axis = 'columns')

# encoding the MSZoning column into 1s and 0s
dummie_train = pd.get_dummies(home_train_data.MSZoning)
merged_train = pd.concat([home_train_data, dummie_train], axis= 'columns')

final_test = merged_test.drop(['MSZoning','C (all)'], axis = 'columns')
final_train = merged_train.drop(['MSZoning','C (all)'], axis = 'columns')

#selecting my predicting features (independent variables)
train_features = ['Id','LotArea','YrSold','MSSubClass', 'RL','RM','FV']
train_X =  final_train[train_features]

#selecting my dependent variable y, which I will use for model validation
train_y =  final_train.SalePrice

#selecting my predicting features (independent variables)
test_features = ['Id','LotArea','YrSold', 'MSSubClass', 'RL','RM','FV']
test_X = final_test[test_features]

#selecting my dependent variable y
test_y = sample_submission.SalePrice

#my model I will use a gradient boosted decision tree
my_model = XGBRegressor(n_estimators=10000)

my_model.fit(train_X,train_y, early_stopping_rounds=7, 
             eval_set=[(test_X, test_y)], verbose=False)

predictions = my_model.predict(test_X)

Id = final_test['Id']


my_submission = pd.DataFrame({'Id':Id, 'SalePrice': predictions})
my_submission.to_csv('submission.csv', index=False)

mae = mean_absolute_error(predictions, test_y)
print('Mean Absolute Error is', mae)

