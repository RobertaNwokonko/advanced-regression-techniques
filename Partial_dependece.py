import pandas as pd
from sklearn.impute import SimpleImputer
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import Imputer
from sklearn.ensemble import GradientBoostingRegressor, GradientBoostingClassifier
from sklearn.ensemble.partial_dependence import partial_dependence, plot_partial_dependence
from sklearn.preprocessing import Imputer
from sklearn.ensemble import RandomForestRegressor
from xgboost import XGBRegressor



# Read the data
train_data = pd.read_csv('Dataset1/train.csv')
test_data = pd.read_csv('Dataset1/test.csv')
sample_submission = pd.read_csv('Dataset1/sample_submission.csv')

cols_to_use = ['Id','LotArea','MSSubClass','YrSold','OverallQual','OverallCond', 'YearBuilt','Fireplaces','MasVnrArea', 'BsmtFinSF2','BsmtFinSF1','BsmtUnfSF']

def get_data():
    data = pd.read_csv('Dataset1/train.csv')
    y = data.SalePrice
    X = data[cols_to_use]
    my_imputer = Imputer()
    imputed_X = my_imputer.fit_transform(X)
    return imputed_X, y


X, y = get_data()
my_model = GradientBoostingRegressor()
my_model.fit(X, y)
my_plots = plot_partial_dependence(my_model, 
                                   features=[0,2], 
                                   X=X, 
                                   feature_names=cols_to_use, 
                                   grid_resolution=10)

print(my_plots)


# Read the data
train_data = pd.read_csv(r'/Users/rob/Workspace/Advanced-regression-techniques/Dataset1/train.csv')
test_data = pd.read_csv(r'/Users/rob/Workspace/Advanced-regression-techniques/Dataset1/test.csv')
sample_submission = pd.read_csv(r"/Users/rob/Workspace/Advanced-regression-techniques/Dataset1/sample_submission.csv")


# encoding the MSZoning column into 1s and 0data
dummie_test = pd.get_dummies(test_data, columns = ['MSZoning','FireplaceQu','GarageFinish','SaleCondition','LotShape','LandContour','LotConfig','HouseStyle'], drop_first = True)
# merged_test = pd.concat([test_data, dummie_test], axis = 'columns')

# final_test = merged_test.drop(['MSZoning','C (all)','Alley','OverallQual','OverallCond'], axis = 'columns')
# final_train = merged_train.drop(['MSZoning','C (all)','Alley','OverallQual','OverallCond'], axis = 'columns')

# encoding the MSZoning column into 1s and 0s
dummie_train = pd.get_dummies(train_data, columns = ['MSZoning','FireplaceQu','GarageFinish','SaleCondition','LotShape','LandContour','LotConfig','HouseStyle'], drop_first = True)
# merged_train = pd.concat([home_train_data, dummie_train], axis= 'columns')

# print(dummie_train)

train_features = ['Id','LotArea','MSSubClass','YrSold', 'MSZoning_RL','MSZoning_RM','MSZoning_FV','LotConfig_FR2','LotConfig_FR3','OverallQual','OverallCond','YearBuilt','Fireplaces','LandContour_Lvl','LandContour_Low']
train_X = dummie_train[train_features]
y = train_data.SalePrice

#selecting my predicting features (independent variables)
test_features = ['Id','LotArea','MSSubClass','YrSold', 'MSZoning_RL','MSZoning_RM','MSZoning_FV','LotConfig_FR2','LotConfig_FR3','OverallQual','OverallCond','YearBuilt','Fireplaces','LandContour_Lvl','LandContour_Low']
train_X = dummie_train[train_features]
test_X = dummie_test[test_features]

model = XGBRegressor(n_estimators = 1000)
model.fit(train_X,y)
# model.fit(train_X,y)

my_prediction = model.predict(test_X)
print(my_prediction)


