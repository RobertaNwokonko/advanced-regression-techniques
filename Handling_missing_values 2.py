import pandas as pd
from sklearn.model_selection import train_test_split


test_data = "Dataset1/test.csv"
train_data = 'Dataset1/train.csv'
sample_submission = 'Dataset1/sample_submission.csv'


home_test_data = pd.read_csv(test_data)
home_train_data = pd.read_csv(train_data)
sample_submission = pd.read_csv(sample_submission)

# make an instance of thr original data set (incase I have to Impute into columns)
new_test_data = home_test_data.copy()
new_train_data = home_train_data.copy()
new_sample_submission = sample_submission.copy()

#to find which columns in new_test_set have null values
missing_column = (new_test_data.isnull().sum())
print(missing_column.head())

