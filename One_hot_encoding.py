import pandas as pd
from sklearn.impute import SimpleImputer
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from xgboost import XGBRegressor
from sklearn.ensemble import GradientBoostingRegressor, GradientBoostingClassifier


iowa_test_data = 'Dataset1/test.csv'
iowa_train_data = 'Dataset1/train.csv'
iowa_sample_submission = 'Dataset1/sample_submission.csv'

home_test_data = pd.read_csv(iowa_test_data)
home_train_data = pd.read_csv(iowa_train_data)
# sample_submission = pd.read_csv(iowa_sample_submission)



# encoding the MSZoning column into 1s and 0data
dummie_test = pd.get_dummies(home_test_data, columns = ['MSZoning','FireplaceQu','GarageFinish','SaleCondition','LotShape','LandContour','LotConfig','HouseStyle'], drop_first = True)
# merged_test = pd.concat([test_data, dummie_test], axis = 'columns')

# final_test = merged_test.drop(['MSZoning','C (all)','Alley','OverallQual','OverallCond'], axis = 'columns')
# final_train = merged_train.drop(['MSZoning','C (all)','Alley','OverallQual','OverallCond'], axis = 'columns')

# encoding the MSZoning column into 1s and 0s
dummie_train = pd.get_dummies(home_train_data, columns = ['MSZoning','FireplaceQu','GarageFinish','SaleCondition','LotShape','LandContour','LotConfig','HouseStyle'], drop_first = True)
# merged_train = pd.concat([home_train_data, dummie_train], axis= 'columns')

# print(dummie_train)

train_features = ['Id','LotArea','MSSubClass','YrSold', 'MSZoning_RL','MSZoning_RM','MSZoning_FV','LotConfig_FR2','LotConfig_FR3','OverallQual','OverallCond','YearBuilt','Fireplaces','LandContour_Lvl','LandContour_Low']
train_X = dummie_train[train_features]
y = home_train_data.SalePrice

#selecting my predicting features (independent variables)
test_features = ['Id','LotArea','MSSubClass','YrSold', 'MSZoning_RL','MSZoning_RM','MSZoning_FV','LotConfig_FR2','LotConfig_FR3','OverallQual','OverallCond','YearBuilt','Fireplaces','LandContour_Lvl','LandContour_Low']
train_X = dummie_train[train_features]
test_X = dummie_test[test_features]

model = GradientBoostingRegressor()
model.fit(train_X,y)
# model.fit(train_X,y)

my_prediction = model.predict(test_X)
print(my_prediction)
















# # encoding the MSZoning column into 1s and 0s
# dummie_test = pd.get_dummies(home_test_data.MSZoning)
# merged_test = pd.concat([home_test_data, dummie_test], axis = 'columns')

# # encoding the MSZoning column into 1s and 0data
# dummie_test = pd.get_dummies(home_test_data, columns= ['OverallQual','OverallCond','MSZoning'], drop_first= True)
# merged_test = pd.concat([home_test_data, dummie_test], axis = 'columns')

# # encoding the MSZoning column into 1s and 0s
# dummie_train = pd.get_dummies(home_train_data.MSZoning)
# merged_train = pd.concat([home_train_data, dummie_train], axis= 'columns')

# print(merged_train)


# # final_test = merged_test.drop(['MSZoning','C (all)','Alley'], axis = 'columns')
# # final_train = merged_train.drop(['MSZoning','C (all)','Alley'], axis = 'columns')

# #selecting my predicting features (independent variables)
# train_features = ['Id','LotArea','MSSubClass','YrSold', 'RL','RM','FV','OverallQual','OverallCond', 'YearBuilt','Fireplaces','MasVnrArea', 'BsmtFinSF2','BsmtFinSF1','BsmtUnfSF']
# train_X =  merged_train[train_features]

# # train_X, test_X, train_y, val_y = train_test_split(train_X, train_y random_state = 1)

# #selecting my dependent variable y, which I will use for model validation
# y =  merged_train.SalePrice

# #selecting my predicting features (independent variables)
# test_features = ['Id','LotArea','MSSubClass','YrSold', 'RL','RM','FV','OverallQual','OverallCond', 'YearBuilt','Fireplaces','MasVnrArea', 'BsmtFinSF2','BsmtFinSF1','BsmtUnfSF','YearBuilt','MSSubClass','YearRemodAdd']
# test_X = merged_test[test_features]

# # test_y = sample_submission.SalePrice

# model = RandomForestRegressor()

# model.fit(train_X,y,)

# Id = final_test['Id']

# my_prediction = model.predict(test_X)

# # print(my_prediction)

# my_submission = pd.DataFrame({'Id':Id, 'SalePrice': my_prediction})
# my_submission.to_csv('submission.csv', index=False)